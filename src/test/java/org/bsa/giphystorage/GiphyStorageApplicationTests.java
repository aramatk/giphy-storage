package org.bsa.giphystorage;

import org.bsa.giphystorage.ram.repository.UserGifsStorageRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("dev")
class GiphyStorageApplicationTests {
//	@Autowired
//	UserGifsStorageRepository userGifsStorageRepository;

	@Test
	void contextLoads() {
	}

	@Test
	void clearInnerMemory() {
		//userGifsStorageRepository.deleteAll();
	}
}
