package org.bsa.giphystorage.giphy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bsa.giphystorage.giphy.dto.GiphyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Service
public class GiphyService implements IGiphyService {
    @Autowired
    protected RestTemplate restTemplate;
    @Value(value = "${giphy.api_key}")
    private String GIPHY_API_KEY;

    @Override
    public Optional<GiphyDto> getGiphy(String query) throws GiphyServiceException {
        try {
            String url = "https://api.giphy.com/v1/gifs/search";
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("api_key", GIPHY_API_KEY)
                    .queryParam("q", query);
            ResponseEntity<String> response = restTemplate
                    .getForEntity(builder.toUriString(), String.class);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode data = root.path("data");
            Optional<String> id = data.elements().hasNext() ?
                    Optional.of(data.elements().next().path("id").asText()) :
                    Optional.empty();
            return id.isPresent() ?
                    Optional.of(new GiphyDto(id.get(),
                            String.format("https://i.giphy.com/media/%s/giphy.webp", id.get()))) :
                    Optional.empty();
        } catch (Exception e) {
            throw new GiphyServiceException(e);
        }
    }
}
