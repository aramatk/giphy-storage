package org.bsa.giphystorage.giphy;

import org.bsa.giphystorage.giphy.dto.GiphyDto;

import java.util.Optional;

public interface IGiphyService {
    Optional<GiphyDto> getGiphy(String query) throws GiphyServiceException;
}
