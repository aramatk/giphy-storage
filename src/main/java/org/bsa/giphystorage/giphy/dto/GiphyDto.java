package org.bsa.giphystorage.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GiphyDto {
    private String id;
    private String url;
}
