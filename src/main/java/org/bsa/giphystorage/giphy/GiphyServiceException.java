package org.bsa.giphystorage.giphy;

public class GiphyServiceException extends Exception {
    public GiphyServiceException() {
    }

    public GiphyServiceException(String message) {
        super(message);
    }

    public GiphyServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public GiphyServiceException(Throwable cause) {
        super(cause);
    }

    public GiphyServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
