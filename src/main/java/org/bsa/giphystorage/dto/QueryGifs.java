package org.bsa.giphystorage.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryGifs {
    private String query;
    @JsonProperty(value = "gifs")
    private List<String> gifUrls;

    public QueryGifs(String query) {
        this.query = query;
    }

    public List<String> getGifUrls() {
        if (gifUrls == null) {
            gifUrls = new ArrayList<>();
        }
        return gifUrls;
    }
}
