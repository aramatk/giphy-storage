package org.bsa.giphystorage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryForceRequest {
    private String query;
    private Boolean force;
}
