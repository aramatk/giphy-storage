package org.bsa.giphystorage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserHistory {
    private String date;
    private String query;
    private String gif;
}
