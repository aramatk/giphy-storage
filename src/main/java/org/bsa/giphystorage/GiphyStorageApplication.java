package org.bsa.giphystorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiphyStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiphyStorageApplication.class, args);
	}

}
