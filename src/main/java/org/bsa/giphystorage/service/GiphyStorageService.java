package org.bsa.giphystorage.service;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.bsa.giphystorage.disk.IDiskStorage;
import org.bsa.giphystorage.dto.QueryGifs;
import org.bsa.giphystorage.dto.UserHistory;
import org.bsa.giphystorage.giphy.GiphyServiceException;
import org.bsa.giphystorage.giphy.IGiphyService;
import org.bsa.giphystorage.ram.model.StorageQueryGiphy;
import org.bsa.giphystorage.ram.model.StorageQueryGiphyMapper;
import org.bsa.giphystorage.ram.model.UserGifsStorage;
import org.bsa.giphystorage.ram.repository.UserGifsStorageRepository;
import org.bsa.giphystorage.service.exception.FolderNameIsNotCorrectException;
import org.bsa.giphystorage.service.exception.GiphyStorageServiceException;
import org.bsa.giphystorage.util.GiphyStorageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GiphyStorageService implements IGiphyStorageService {
    @Autowired
    private IDiskStorage diskStorage;
    @Autowired
    private IGiphyService giphyService;
    @Autowired
    private UserGifsStorageRepository storageRepository;

    private static final String FOLDER_NAME_REQ_EXP = "^([a-zA-Z_0-9]+\\.?)*[a-zA-Z_0-9]+$";

    @Override
    public QueryGifs generateGiphy(String userId, String query, Boolean force) throws GiphyStorageServiceException, FolderNameIsNotCorrectException {
        if (!isDirNameValid(userId) || !isDirNameValid(query)) {
            throw  new FolderNameIsNotCorrectException("The user id and query is not correct!");
        }
        if (Boolean.TRUE.equals(force)) {
            return generateGifFromService(userId, query);
        } else {
            var queryGiphy = saveGifFromCacheToUserFolder(userId, query);
            if (queryGiphy.isPresent()) {
                return queryGiphy.get();
            } else {
                return generateGifFromService(userId, query);
            }
        }
    }

    private QueryGifs generateGifFromService(String userId, String query)
            throws GiphyStorageServiceException, FolderNameIsNotCorrectException {
        saveToCacheFromGiphyService(query);
        var gif = saveGifFromCacheToUserFolder(userId, query);
        return gif.isPresent()? gif.get() : null;
    }

    private Optional<QueryGifs> saveGifFromCacheToUserFolder(String userId, String query) throws GiphyStorageServiceException {
        try {
            var gifPath = diskStorage.saveGifFromCache(userId, query);
            if (gifPath.isPresent()) {
                diskStorage.saveHistory(userId, query, gifPath.get());
                var storageQueryGiphy = refreshMemoryData(userId, query, gifPath.get());
                return Optional.of(StorageQueryGiphyMapper.MAPPER.storageQueryGiphyToQueryGiphy(storageQueryGiphy));
            }
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
        return Optional.empty();
    }

    private StorageQueryGiphy refreshMemoryData(String userId, String query, String path) {
        Optional<UserGifsStorage> userGifsStorageOptional = storageRepository.findById(userId);
        if (userGifsStorageOptional.isPresent()) {
            return updateExistingUserGifsStorage(userId, query, path, userGifsStorageOptional.get());
        } else {
            var storageQueryGiphy = new StorageQueryGiphy(query, new HashSet<>(Arrays.asList(path)));
            var userGifsStorage = UserGifsStorage.builder()
                    .id(userId)
                    .gifList(Arrays.asList(storageQueryGiphy))
                    .build();
            storageRepository.save(userGifsStorage);
            return storageQueryGiphy;
        }
    }

    private StorageQueryGiphy updateExistingUserGifsStorage(String userId, String query, String path,
                                                            UserGifsStorage userGifsStorage) {
        var storageQueryGiphyOptional = userGifsStorage.getGifList().stream()
                .filter(i -> i.getQuery().equals(query)).findFirst();
        StorageQueryGiphy storageQueryGiphy = null;
        if (storageQueryGiphyOptional.isPresent()) {
            storageQueryGiphy = storageQueryGiphyOptional.get();
            storageQueryGiphy.getGifUrls().add(path);
        } else {
            storageQueryGiphy = new StorageQueryGiphy(query, new HashSet<>(Arrays.asList(path)));
            userGifsStorage.getGifList().add(storageQueryGiphy);
        }
        storageRepository.save(userGifsStorage);
        return storageQueryGiphy;
    }

    @Override
    public void saveToCacheFromGiphyService(String query) throws GiphyStorageServiceException, FolderNameIsNotCorrectException {
        if (!isDirNameValid(query)) {
            throw  new FolderNameIsNotCorrectException("Query is not correct!");
        }
        try {
            var gifUrl = giphyService.getGiphy(query);
            if (gifUrl.isPresent()) {
                diskStorage.saveGifToCache(gifUrl.get().getId(), gifUrl.get().getUrl(), query);
            }
        } catch (GiphyServiceException | IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public Collection<QueryGifs> getCacheGifs(String query) throws GiphyStorageServiceException {
        try {
            return diskStorage.getCacheGifs(query);
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public Collection<String> getCacheGifs() throws GiphyStorageServiceException {
        try {
            Collection<QueryGifs> queryGifs = diskStorage.getCacheGifs(null);
            return queryGifs.stream()
                    .flatMap(i -> i.getGifUrls().stream())
                    .collect(Collectors.toSet());

        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public Collection<QueryGifs> getUserGifs(String userId) throws GiphyStorageServiceException {
        try {
            return diskStorage.getUserGifs(userId, null);
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public void cleanCache() throws GiphyStorageServiceException {
        try {
            diskStorage.cleanCache();
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public void cleanHistory(String userId) throws GiphyStorageServiceException {
        try {
            diskStorage.cleanHistory(userId);
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public void cleanMemory(String userId, String query) {
        if (StringUtils.isBlank(query)) {
            storageRepository.deleteById(userId);
        } else {
            var userMemoryStorage = storageRepository.findById(userId);
            if (userMemoryStorage.isPresent()) {
                var gifList = userMemoryStorage.get().getGifList().stream()
                        .filter(i -> !i.getQuery().equals(query)).collect(Collectors.toList());
                userMemoryStorage.get().setGifList(gifList);
                storageRepository.save(userMemoryStorage.get());
            }
        }
    }

    @Override
    public void cleanAll(String userId) throws GiphyStorageServiceException {
        try {
            storageRepository.deleteById(userId);
            diskStorage.clean(userId);
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public List<UserHistory> getUserHistory(String userId) throws GiphyStorageServiceException {
        try {
            List<String> historyList = diskStorage.getUserHistory(userId);
            return historyList.stream().map(i -> {
                String[] h = i.split(GiphyStorageUtils.HISTORY_DATA_SEPARATOR);
                return new UserHistory(h[0], h[1], h[2]);
            }).collect(Collectors.toList());
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    @Override
    public String getUserGifUrl(String userId, String query, Boolean force) throws GiphyStorageServiceException {
        try {
            if (Boolean.TRUE.equals(force)) {
                return getGifUrlFromDiskAndSaveToMemory(userId, query);
            } else {
                String gifUrl = getGifUrlFromMemory(userId, query);
                gifUrl = gifUrl != null ? gifUrl : getGifUrlFromDiskAndSaveToMemory(userId, query);
                return gifUrl;
            }
        } catch (IOException e) {
            throw new GiphyStorageServiceException(e);
        }
    }

    private String getGifUrlFromDiskAndSaveToMemory(String userId, String query) throws IOException {
        Collection<QueryGifs> queryGifs = diskStorage.getUserGifs(userId, query);
        String gifUrl =
                CollectionUtils.isNotEmpty(queryGifs) ? queryGifs.iterator().next().getGifUrls().get(0) : null;
        if (gifUrl != null) {
            refreshMemoryData(userId, query, gifUrl);
        }
        return gifUrl;
    }

    private String getGifUrlFromMemory(String userId, String query) {
        var userMemoryStorage = storageRepository.findById(userId);
        Optional<StorageQueryGiphy> storageQueryGiphy = userMemoryStorage.isPresent() ?
                userMemoryStorage.get().getGifList().stream().filter(i -> i.getQuery().equals(query)).findFirst() :
                Optional.empty();
        return storageQueryGiphy.isPresent() ? storageQueryGiphy.get().getGifUrls().iterator().next() : null;
    }

    public boolean isDirNameValid(String name) {
        return name.matches(FOLDER_NAME_REQ_EXP);
    }
}