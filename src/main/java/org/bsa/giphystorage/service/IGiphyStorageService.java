package org.bsa.giphystorage.service;

import org.bsa.giphystorage.dto.QueryGifs;
import org.bsa.giphystorage.dto.UserHistory;
import org.bsa.giphystorage.service.exception.FolderNameIsNotCorrectException;
import org.bsa.giphystorage.service.exception.GiphyStorageServiceException;

import java.util.Collection;
import java.util.List;

public interface IGiphyStorageService {
    QueryGifs generateGiphy(String userId, String query, Boolean force)
            throws GiphyStorageServiceException, FolderNameIsNotCorrectException;

    void saveToCacheFromGiphyService(String query) throws GiphyStorageServiceException, FolderNameIsNotCorrectException;

    Collection<QueryGifs> getCacheGifs(String query) throws GiphyStorageServiceException;

    Collection<String> getCacheGifs() throws GiphyStorageServiceException;

    Collection<QueryGifs> getUserGifs(String userId) throws GiphyStorageServiceException;

    void cleanCache() throws GiphyStorageServiceException;

    void cleanHistory(String userId) throws GiphyStorageServiceException;

    void cleanMemory(String userId, String query);

    void cleanAll(String userId) throws GiphyStorageServiceException;

    List<UserHistory> getUserHistory(String userId) throws GiphyStorageServiceException;

    String getUserGifUrl(String userId, String query, Boolean force) throws GiphyStorageServiceException;
}
