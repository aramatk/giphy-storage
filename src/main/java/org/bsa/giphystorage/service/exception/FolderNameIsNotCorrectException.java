package org.bsa.giphystorage.service.exception;

public class FolderNameIsNotCorrectException extends Exception {
    public FolderNameIsNotCorrectException() {
    }

    public FolderNameIsNotCorrectException(String message) {
        super(message);
    }

    public FolderNameIsNotCorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public FolderNameIsNotCorrectException(Throwable cause) {
        super(cause);
    }

    public FolderNameIsNotCorrectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
