package org.bsa.giphystorage.service.exception;

public class GiphyStorageServiceException extends Exception {
    public GiphyStorageServiceException() {
    }

    public GiphyStorageServiceException(String message) {
        super(message);
    }

    public GiphyStorageServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public GiphyStorageServiceException(Throwable cause) {
        super(cause);
    }

    public GiphyStorageServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
