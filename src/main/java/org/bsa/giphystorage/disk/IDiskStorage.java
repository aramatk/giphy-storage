package org.bsa.giphystorage.disk;

import org.bsa.giphystorage.dto.QueryGifs;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IDiskStorage {

    Optional<String> saveGifFromCache(String userId, String query) throws IOException;

    Optional<String> getGifPath(String userId, String query);

    void saveGifToCache(String gifId, String gifUrl, String query) throws IOException;

    void saveHistory(String userId, String query, String path) throws IOException;

    Collection<QueryGifs> getCacheGifs(String query)  throws IOException;

    Collection<QueryGifs> getUserGifs(String userId, String query)  throws IOException;

    void cleanCache() throws IOException;

    void cleanHistory(String userId) throws IOException;

    void clean(String userId) throws IOException;

    List<String> getUserHistory(String userId) throws IOException;
}
