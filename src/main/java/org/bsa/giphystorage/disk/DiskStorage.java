package org.bsa.giphystorage.disk;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.bsa.giphystorage.dto.QueryGifs;
import org.bsa.giphystorage.util.GiphyStorageUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DiskStorage implements IDiskStorage {
    @Value(value = "${storage.base.dir}")
    private String STORAGE_BASE_DIR;
    @Value(value = "${base.gifs.url}")
    private String GIFS_URL;
    @Value(value = "${storage.cache.dir}")
    private String STORAGE_CACHE_DIR;
    @Value(value = "${storage.history.filename}")
    private String STORAGE_HISTORY_FILE_NAME;

    @Override
    public Optional<String> saveGifFromCache(String userId, String query) throws IOException {
        File cacheDir = getCacheDir(query);
        var gifFile = getGifFromDir(cacheDir);
        if (gifFile.isPresent()) {
            File destFile = new File(String.format("%s%s%s%s%s%s", STORAGE_BASE_DIR, userId, File.separator, query,
                    File.separator, gifFile.get().getName()));
            FileUtils.copyFile(gifFile.get(), destFile);
            return getUrlPath(userId, query, gifFile.get());
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> getGifPath(String userId, String query) {
        File userDir = getUserDir(userId, query);
        var gifFile = getGifFromDir(userDir);
        return gifFile.isPresent() ? getUrlPath(userId, query, gifFile.get()) : Optional.empty();
    }

    private Optional<File> getGifFromDir(File dir) {
        List<File> files = getGifsFromDir(dir);
        return CollectionUtils.isNotEmpty(files) ? Optional.of(files.get(0)) : Optional.empty();
    }

    private List<File> getGifsFromDir(File dir) {
        try {
            String[] extensions = new String[]{"gif"};
            List<File> files = (List<File>) FileUtils.listFiles(dir, extensions, false);
            return files;
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    private Optional<String> getUrlPath(String userId, String query, File gifFile) {
        File urlFilePath = new File(String.format("%s%s%s%s%s%s", GIFS_URL, userId, File.separator, query,
                File.separator, gifFile.getName()));
        return Optional.of(urlFilePath.getPath());
    }

    private Optional<String> getCacheUrlPath(String query, File gifFile) {
        return getUrlPath(STORAGE_CACHE_DIR, query, gifFile);
    }

    public void saveGifToCache(String gifId, String gifUrl, String query) throws IOException {
        File gifFile = new File(String.format("%s%s%s%s%s%s%s", STORAGE_BASE_DIR, STORAGE_CACHE_DIR, File.separator,
                query, File.separator, gifId, ".gif"));
        if (!gifFile.exists()) {
            FileUtils.copyURLToFile(new URL(gifUrl), gifFile);
        }
    }

    @Override
    public void saveHistory(String userId, String query, String path) throws IOException {
        File historyFile = getHistoryFile(userId);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = format.format(new Date());
        String line = StringUtils.join(Arrays.asList(dateString, query, path), GiphyStorageUtils.HISTORY_DATA_SEPARATOR);
        FileUtils.writeLines(historyFile, Arrays.asList(line), true);
    }

    @Override
    public Collection<QueryGifs> getCacheGifs(String query) {
        File cacheDir = getCacheDir(query);
        return getGifs(cacheDir, query);
    }

    @Override
    public Collection<QueryGifs> getUserGifs(String userId, String query) {
        File userDir = getUserDir(userId, query);
        return getGifs(userDir, query);
    }

    private List<QueryGifs> getGifs(File dir, String query) {
        List<QueryGifs> queryGifsList = new ArrayList<>();
        if (StringUtils.isBlank(query)) {
            Collection<File> directories = FileUtils.listFilesAndDirs(dir,
                    new NotFileFilter(TrueFileFilter.INSTANCE), DirectoryFileFilter.DIRECTORY);
            directories = directories.stream().filter(i -> !i.equals(dir)).collect(Collectors.toList());
            for (File dirFile : directories) {
                createAndAddQueryGifs(queryGifsList, dirFile.getName(), dirFile);
            }
        } else {
            createAndAddQueryGifs(queryGifsList, query, dir);
        }
        return queryGifsList;
    }

    private void createAndAddQueryGifs(List<QueryGifs> queryGifsList, String query, File dir) {
        QueryGifs queryGifs = new QueryGifs(query);
        Collection<File> files = getGifsFromDir(dir);
        for (File file : files) {
            Optional<String> path = getCacheUrlPath(query, file);
            if (path.isPresent()) {
                queryGifs.getGifUrls().add(path.get());
            }
        }
        if (CollectionUtils.isNotEmpty(queryGifs.getGifUrls())) {
            queryGifsList.add(queryGifs);
        }
    }

    @Override
    public void cleanCache() throws IOException {
        File cacheDir = getCacheDir();
        FileUtils.cleanDirectory(cacheDir);
    }

    @Override
    public void cleanHistory(String userId) throws IOException {
        File historyFile = getHistoryFile(userId);
        FileUtils.writeLines(historyFile, null);
    }

    @Override
    public void clean(String userId) throws IOException {
        File userDir = getUserDir(userId);
        if(userDir.exists()) {
            FileUtils.cleanDirectory(userDir);
            userDir.delete();
        }
    }

    @Override
    public List<String> getUserHistory(String userId) throws IOException {
        File historyFile = getHistoryFile(userId);
        return FileUtils.readLines(historyFile, Charset.defaultCharset());
    }

    private File getUserDir(String userId, String query) {
        return StringUtils.isBlank(query) ?
                new File(String.format("%s%s", STORAGE_BASE_DIR, userId)) :
                new File(String.format("%s%s%s%s%s", STORAGE_BASE_DIR, userId, File.separator, query, File.separator));
    }

    private File getHistoryFile(String userId) {
        File historyFile = new File(String.format("%s%s%s%s", STORAGE_BASE_DIR, userId, File.separator,
                STORAGE_HISTORY_FILE_NAME));
        return historyFile;
    }


    private File getCacheDir(String query) {
        return StringUtils.isBlank(query) ?
                new File(String.format("%s%s", STORAGE_BASE_DIR, STORAGE_CACHE_DIR)) :
                new File(String.format("%s%s%s%s", STORAGE_BASE_DIR, STORAGE_CACHE_DIR, File.separator, query));
    }

    private File getCacheDir() {
        return getCacheDir(null);
    }

    private File getUserDir(String userId) {
        return getUserDir(userId, null);
    }
}
