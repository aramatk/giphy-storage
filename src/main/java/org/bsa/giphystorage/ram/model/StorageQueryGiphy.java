package org.bsa.giphystorage.ram.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StorageQueryGiphy implements Serializable {
    private String query;
    private Set<String> gifUrls;

    public Set<String> getGifUrls() {
        if (gifUrls == null) {
            return new HashSet<>();
        }
        return gifUrls;
    }
}
