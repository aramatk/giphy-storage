package org.bsa.giphystorage.ram.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RedisHash("RedisStorage")
public class UserGifsStorage implements Serializable {
    private String id;
    private List<StorageQueryGiphy> gifList;

    public List<StorageQueryGiphy> getGifList() {
        if (gifList == null) {
            return new ArrayList<>();
        }
        return gifList;
    }
}
