package org.bsa.giphystorage.ram.model;

import org.bsa.giphystorage.dto.QueryGifs;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StorageQueryGiphyMapper {
    StorageQueryGiphyMapper MAPPER = Mappers.getMapper(StorageQueryGiphyMapper.class);

    QueryGifs storageQueryGiphyToQueryGiphy(StorageQueryGiphy storageQueryGiphy);
}
