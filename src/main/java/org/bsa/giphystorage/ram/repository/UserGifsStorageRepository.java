package org.bsa.giphystorage.ram.repository;

import org.bsa.giphystorage.ram.model.UserGifsStorage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGifsStorageRepository extends CrudRepository<UserGifsStorage, String> {
}

