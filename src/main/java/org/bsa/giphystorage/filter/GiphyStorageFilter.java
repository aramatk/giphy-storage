package org.bsa.giphystorage.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GiphyStorageFilter implements Filter {
    private static final String X_BSA_GIPHY_HEADER_KEY = "X-BSA-GIPHY";
    @Value(value = "${gifs.url}")
    private String GIF_RESOURCES;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String path = req.getRequestURI();

        if(path.contains(GIF_RESOURCES) || path.contains("actuator")){
            chain.doFilter(request, response);
            return;
        }

        String key = req.getHeader(X_BSA_GIPHY_HEADER_KEY);
        if (key != null) {
            chain.doFilter(request, response);
        } else {
            HttpServletResponse resp = (HttpServletResponse) response;
            String error = "Invalid API KEY";

            resp.reset();
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.setContentLength(error.length());
            response.getWriter().write(error);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}