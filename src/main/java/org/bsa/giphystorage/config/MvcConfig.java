package org.bsa.giphystorage.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Value(value = "${storage.resource.location}")
    private String STORAGE_LOCATION;
    @Value(value = "${gifs.url}")
    private String GIF_RESOURCES;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler(String.format("/%s/**", GIF_RESOURCES))
                .addResourceLocations(STORAGE_LOCATION);
    }
}