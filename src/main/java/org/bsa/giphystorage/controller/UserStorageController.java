package org.bsa.giphystorage.controller;


import org.apache.commons.lang3.StringUtils;
import org.bsa.giphystorage.dto.QueryForceRequest;
import org.bsa.giphystorage.dto.QueryGifs;
import org.bsa.giphystorage.dto.UserHistory;
import org.bsa.giphystorage.service.exception.FolderNameIsNotCorrectException;
import org.bsa.giphystorage.service.exception.GiphyStorageServiceException;
import org.bsa.giphystorage.service.IGiphyStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@RequestMapping("/user")
public class UserStorageController {
    @Autowired
    private IGiphyStorageService giphyStorageService;

    @GetMapping("/{id}/all")
    public Collection<QueryGifs> getGifs(@PathVariable String id) {
        try {
            return giphyStorageService.getUserGifs(id);
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/search")
    public String searchGif(@PathVariable String id, @RequestParam String query,
                            @RequestParam(required = false) Boolean force) {
        try {
            String gifUrl = giphyStorageService.getUserGifUrl(id, query, force);
            if (StringUtils.isBlank(gifUrl)) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            return gifUrl;
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/history")
    public Collection<UserHistory> getHistory(@PathVariable String id) {
        try {
            return giphyStorageService.getUserHistory(id);
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/{id}/generate")
    public String generate(@PathVariable String id, @RequestBody QueryForceRequest q) {
        try {
            QueryGifs queryGifs = giphyStorageService.generateGiphy(id, q.getQuery(), q.getForce());
            return queryGifs != null? queryGifs.getGifUrls().get(0) : null;
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (FolderNameIsNotCorrectException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}/history/clean")
    public void historyClean(@PathVariable String id) {
        try {
            giphyStorageService.cleanHistory(id);
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}/reset")
    public void memoryClean(@PathVariable String id, @RequestParam(required = false) String query) {
        giphyStorageService.cleanMemory(id, query);
    }

    @DeleteMapping("/{id}/clean")
    public void totalClean(@PathVariable String id) {
        try {
            giphyStorageService.cleanAll(id);
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
