package org.bsa.giphystorage.controller;


import org.bsa.giphystorage.dto.QueryGifs;
import org.bsa.giphystorage.dto.QueryRequest;
import org.bsa.giphystorage.service.IGiphyStorageService;
import org.bsa.giphystorage.service.exception.FolderNameIsNotCorrectException;
import org.bsa.giphystorage.service.exception.GiphyStorageServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
public class CacheStorageController {
    @Autowired
    private IGiphyStorageService giphyStorageService;

    @GetMapping("/cache")
    public Collection<QueryGifs> get(@RequestParam(required = false) String query) {
        try {
            return giphyStorageService.getCacheGifs(query);
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cache/generate")
    public Collection<QueryGifs> generate(@RequestBody QueryRequest queryRequest) {
        try {
            giphyStorageService.saveToCacheFromGiphyService(queryRequest.getQuery());
            return giphyStorageService.getCacheGifs(queryRequest.getQuery());
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (FolderNameIsNotCorrectException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/cache")
    public void delete() {
        try {
            giphyStorageService.cleanCache();
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/gifs")
    public Collection<String> get() {
        try {
            return giphyStorageService.getCacheGifs();
        } catch (GiphyStorageServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
